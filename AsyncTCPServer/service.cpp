#include "service.h"


Service::Service(std::shared_ptr<asio::ip::tcp::socket> sock) :
    m_sock(sock)
{}

void Service::StartHandling() {
    asio::async_read_until(*m_sock.get(),
                           m_request,
                           '\n',
                           [this](
                           const std::error_code& ec,
                           std::size_t bytes_transferred)
    {
        onRequestReceived(ec, bytes_transferred);
    });
}

void Service::onRequestReceived(const std::error_code &ec, std::size_t bytes_transferred) {
    if (ec) {
        std::cout << "Error occured! Error code = "
                  << ec.value()
                  << ". Message: " << ec.message();
        onFinish();
        return;
    }

    // Process the request.
    m_response = ProcessRequest(m_request);
    // Initiate asynchronous write operation.
    asio::async_write(*m_sock.get(),
                      asio::buffer(m_response),
                      [this](
                      const std::error_code& ec,
                      std::size_t bytes_transferred)
    {
        onResponseSent(ec,
                       bytes_transferred);
    });
}

void Service::onResponseSent(const std::error_code &ec, std::size_t bytes_transferred) {
    if (ec) {
        std::cout << "Error occured! Error code = "
                  << ec.value()
                  << ". Message: " << ec.message();
    }
    onFinish();
}

void Service::onFinish() {
    delete this;
}

std::string Service::ProcessRequest(asio::streambuf &request) {
    // In this method we parse the request, process it
    // and prepare the request.
    // Emulate CPU-consuming operations.
    int i = 0;
    while (i != 1000000)
        i++;
    // Emulate operations that block the thread
    // (e.g. synch I/O operations).
    std::this_thread::sleep_for(
                std::chrono::milliseconds(100));
    // Prepare and return the response message.
    std::string response = "Response\n";
    return response;
}
