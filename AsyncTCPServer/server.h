#ifndef SERVER_H
#define SERVER_H

#include <thread>
#include <asio.hpp>
#include <memory>
#include "acceptor.h"

class Server {
public:
    Server();
    // Start the server.
    void Start(unsigned short port_num,
               unsigned int thread_pool_size);
    // Stop the server.
    void Stop();

private:
    asio::io_service m_ios;
    std::unique_ptr<asio::io_service::work> m_work;
    std::unique_ptr<Acceptor> acc;
    std::vector<std::unique_ptr<std::thread>> m_thread_pool;
};


#endif // SERVER_H
