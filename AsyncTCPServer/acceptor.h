#ifndef ACCEPTOR_H
#define ACCEPTOR_H

#include <asio.hpp>
#include <memory>
#include <system_error>
#include "service.h"

class Acceptor {

public:
    Acceptor(asio::io_service&ios, unsigned short port_num);

    // Start accepting incoming connection requests.
    void Start();

    // Stop accepting incoming connection requests.
    void Stop();

private:
    void InitAccept();
    void onAccept(const std::error_code& ec,
                  std::shared_ptr<asio::ip::tcp::socket> sock);

private:
    asio::io_service&m_ios;
    asio::ip::tcp::acceptor m_acceptor;
    std::atomic<bool>m_isStopped;
};
#endif // ACCEPTOR_H
