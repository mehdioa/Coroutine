TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lpthread

SOURCES += main.cpp \
    service.cpp \
    acceptor.cpp \
    server.cpp

HEADERS += \
    service.h \
    acceptor.h \
    server.h
