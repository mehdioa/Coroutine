#ifndef SERVICE_H
#define SERVICE_H

#include <memory>
#include <asio.hpp>
#include <system_error>
#include <iostream>
#include <thread>

class Service {
public:
    Service(std::shared_ptr<asio::ip::tcp::socket> sock);

    void StartHandling();

private:
    void onRequestReceived(const std::error_code& ec,
                           std::size_t bytes_transferred);

    void onResponseSent(const std::error_code& ec,
                        std::size_t bytes_transferred);

    // Here we perform the cleanup.
    void onFinish();

    std::string ProcessRequest(asio::streambuf& request);

private:
    std::shared_ptr<asio::ip::tcp::socket> m_sock;
    std::string m_response;
    asio::streambuf m_request;

};




#endif // SERVICE_H
