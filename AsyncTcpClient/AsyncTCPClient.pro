TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
LIBS += -lpthread

SOURCES += main.cpp \
    asynctcpclient.cpp \
    session.cpp

HEADERS += \
    asynctcpclient.h \
    session.h
