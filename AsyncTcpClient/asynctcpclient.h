#ifndef ASYNCTCPCLIENT_H
#define ASYNCTCPCLIENT_H


//#include <boost/predef.h> // Tools to identify the OS.

// We need this to enable cancelling of I/O operations on
// Windows XP, Windows Server 2003 and earlier.
// Refer to "http://www.boost.org/doc/libs/1_58_0/
// doc/html/boost_asio/reference/basic_stream_socket/
// cancel/overload1.html" for details.
#ifdef BOOST_OS_WINDOWS
#define _WIN32_WINNT 0x0501

#if _WIN32_WINNT <= 0x0502 // Windows Server 2003 or earlier.
    #define BOOST_ASIO_DISABLE_IOCP
    #define BOOST_ASIO_ENABLE_CANCELIO
#endif
#endif

#include <asio.hpp>

#include <thread>
#include <mutex>
#include <memory>
#include <iostream>
#include "session.h"



class AsyncTCPClient {
public:
    AsyncTCPClient();
    AsyncTCPClient( const AsyncTCPClient& ) = delete;

    void emulateLongComputationOp(
        unsigned int duration_sec,
        const std::string& raw_ip_address,
        unsigned short port_num,
        Callback callback,
        unsigned int request_id);

    // Cancels the request.
    void cancelRequest(unsigned int request_id);

    void close();

private:
    void onRequestComplete(std::shared_ptr<Session> session);

private:
    asio::io_service m_ios;
    std::map<int, std::shared_ptr<Session>> m_active_sessions;
    std::mutex m_active_sessions_guard;
    std::unique_ptr<asio::io_service::work> m_work;
    std::unique_ptr<std::thread> m_thread;
};

#endif
